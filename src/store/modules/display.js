export default {
  namespaced: true,
  state: {
    showSidebar: localStorage.getItem("settings") !== null ? JSON.parse(localStorage.getItem("settings")).showSidebar : true,
    showAddTag: localStorage.getItem("settings") !== null ? JSON.parse(localStorage.getItem("settings")).showAddTag : false,
    softwaresViewList: localStorage.getItem("settings") !== null ? JSON.parse(localStorage.getItem("settings")).softwaresViewList : true,
  },
  actions: {
    toggleSidebar({
      commit
    }) {
      commit('toggleSidebar');
      commit('saveSettings');
    },
    toggleSoftwaresView({
      commit
    }) {
      commit('toggleSoftwaresView');
      commit('saveSettings');
    },
    toggleAddTag({
      commit
    }) {
      commit('toggleAddTag');
      commit('saveSettings');
    },
  },
  mutations: {
    toggleSidebar: (state) => {
      state.showSidebar = !state.showSidebar;
    },
    toggleAddTag: (state) => {
      state.showAddTag = !state.showAddTag;
    },
    toggleSoftwaresView: (state) => {
      state.softwaresViewList = !state.softwaresViewList;
    },
    saveSettings: (state) => {
      localStorage.setItem("settings", JSON.stringify(state))
    }
  }
}
