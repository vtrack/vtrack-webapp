export default {
  namespaced: true,
  state: {
    list: [],
    selected: []
  },
  actions: {
    load({
      commit
    }, data) {
      commit('load', data);
    },
    set({
      commit
    }, data) {
      commit('set', data);
    },
    reset({
      commit
    }) {
      commit('reset');
    }
  },
  mutations: {
    load: (state, data) => {
      state.list = data;
    },
    set: (state, data) => {
      state.selected = data;
    },
    reset: (state) => {
      state.selected = [];
    }
  }
}
