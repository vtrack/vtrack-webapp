export default {
  namespaced: true,
  state: {
    logged: localStorage.getItem("logged") === undefined ? false : localStorage.getItem("logged"),
    token: localStorage.getItem("token")
  },
  getters: {
    tokenData: state => {
      if (state.logged) {
        return JSON.parse(atob(state.token.split('.')[1]));
      }
      return null;
    },
    userID: (state, getters) => {
      return getters.tokenData !== null ? getters.tokenData.id : undefined;
    },
    isAdmin: (state, getters) => {
      return getters.tokenData !== null ? getters.tokenData.roles.includes("admin") : false
    }
  },
  actions: {
    login({
      commit
    }, token) {
      commit('login', token);
    },
    logout({
      commit
    }) {
      commit('logout');
    }
  },
  mutations: {
    login: (state, token) => {
      state.logged = true;
      state.token = token;
      localStorage.setItem("token", token);
      localStorage.setItem("logged", true);
    },
    logout: (state) => {
      state.logged = false;
      state.token = '';
      localStorage.removeItem("token");
      localStorage.removeItem("logged");
    }
  }
}
