export default {
  namespaced: true,
  state: {
    list: [],
  },
  actions: {
    new({
      commit
    }, alert) {
      commit('push', alert);

      setTimeout(function() {
        commit('splice', alert);
      }, alert.delay);
    },
    delete({
      commit
    }, alert) {
      commit('splice', alert);
    }
  },
  mutations: {
    push(state, alert) {
      state.list.push(alert);
    },
    splice(state, alert) {
      var index = state.list.indexOf(alert)
      if (index !== -1) {
        state.list.splice(index, 1);
      }
    }
  }
}
