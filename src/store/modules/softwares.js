export default {
  namespaced: true,
  state: {
    list: [],
  },
  actions: {
    load({
      commit
    }, data) {
      commit('load', data);
    }
  },
  mutations: {
    load: (state, data) => {
      state.list = data;
    }
  }
}
