import Vue from 'vue'
import Vuex from 'vuex'

import alerts from '@/store/modules/alerts'
import auth from '@/store/modules/auth'
import categories from '@/store/modules/categories'
import display from '@/store/modules/display'
import server from '@/store/modules/server'
import softwares from '@/store/modules/softwares'
import subscriptions from '@/store/modules/subscriptions'
import tags from '@/store/modules/tags'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  strict: debug,
  modules: {
    alerts,
    auth,
    categories,
    display,
    server,
    softwares,
    subscriptions,
    tags
  }
})
