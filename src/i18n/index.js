import Vue from 'vue'
import VueI18n from 'vue-i18n'
import enUS from '@/i18n/en_US.json'
import frFR from '@/i18n/fr_FR.json'

Vue.use(VueI18n)

const messages = {
  "en-us": {
    ...enUS
  },
  "fr-fr": {
    ...frFR
  }
}

export default new VueI18n({
  fallbackLocale: 'en-us',
  locale: 'en-us',
  messages,
})
