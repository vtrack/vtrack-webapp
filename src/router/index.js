import Vue from 'vue'
import Router from 'vue-router'

import AuthWrapper from '@/components/wrapper/AuthWrapper'
import Login from '@/views/auth/Login'
import Register from '@/views/auth/Register'
import ResetPassword from '@/views/auth/ResetPassword'

import MainWrapper from '@/components/wrapper/MainWrapper'
import Search from '@/views/main/Search'
import Settings from '@/views/main/Settings'
import SoftwareAdd from '@/views/main/SoftwareAdd'
import SoftwareEdit from '@/views/main/SoftwareEdit'
import Subscriptions from '@/views/main/Subscriptions'

import Admin from '@/views/admin/Admin'

import NotFound from '@/views/NotFound'

import store from '@/store';

Vue.use(Router)

let router = new Router({
  base: Vue.config.baseUrl,
  mode: 'history',
  linkActiveClass: "active",
  routes: [{
      path: '',
      component: MainWrapper,
      meta: { requiresAuth: true },
      children: [
        { path: '', component: Subscriptions, name: "subscriptions" },
        { path: '/search', component: Search, name: "search" },
        { path: '/software/add', component: SoftwareAdd, name: "software-add" },
        { path: '/software/edit/:id', component: SoftwareEdit, name: "software-edit" },
        { path: '/settings', component: Settings, name: "settings" },
        { path: '/admin', component: Admin, name: "admin", meta: { isAdmin: true } },
      ]
    },
    {
      path: '',
      component: AuthWrapper,
      children: [
        { path: '/login', component: Login, name: "login" },
        { path: '/register', component: Register, name: "register" },
        { path: '/reset_password', component: ResetPassword, name: "reset_password" },
      ]
    },
    { path: '*', component: NotFound }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) { // require logged
    if (store.state.auth.logged) { // if is logged
      let user = store.getters['auth/tokenData'];
      if (to.matched.some(record => record.meta.isAdmin)) { // require admin
        if (user.roles.includes("admin")) { // if is admin
          next()
        } else { // if is not admin
          next({ name: 'subscriptions' })
        }
      } else { // do not require admin
        next();
      }
    } else { // if is not logged
      next({
        name: 'login',
        query: {
          redirect: to.fullPath
        }
      });
    }
  } else { // do not require logged
    next();
  }
})

export default router
