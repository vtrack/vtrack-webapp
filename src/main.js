import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

// Vue config
var isDev = process.env.NODE_ENV !== 'production';
Vue.config.productionTip = isDev;
Vue.config.baseUrl = process.env.BASE_URL ? process.env.BASE_URL : '';

import 'bootstrap/scss/bootstrap.scss';
import 'material-icons/iconfont/material-icons.scss';

import i18n from '@/i18n';
import alerts from '@/mixin/alerts';
import router from '@/router';
import store from '@/store';

import '@/styles.scss';

import App from '@/App.vue';

import {
  capitalize
} from '@/filters';

Vue.filter('capitalize', capitalize);
Vue.mixin(alerts)

Vue.use(VueAxios, axios.create({
  baseURL: isDev ? 'http://localhost:4000/api/v1' : '/api/v1',
}));

Vue.axios.interceptors.request.use(
  (config) => {
    if (store.state.auth.logged) {
      config.headers['Authorization'] = 'Bearer ' + store.state.auth.token
    }
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

Vue.axios.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    switch (error.response.status) {
      case 401:
        router.push({ name: 'login' });
        break;
      default:
        // eslint-disable-next-line
        console.error(error.response);
    }
    return Promise.reject(error)
  }
)



new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app');
