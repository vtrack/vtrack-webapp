const newAlert = function(type, message) {
  return {
    type: type,
    message: message,
    delay: 4000,
    when: Date.now()
  }
}

export default {
  methods: {
    newSuccess(message) {
      return newAlert('success', message);
    },
    newInfo(message) {
      return newAlert('info', message);
    },
    newWarning(message) {
      return newAlert('warning', message);
    },
    newDanger(message) {
      return newAlert('danger', message);
    }
  }
}
